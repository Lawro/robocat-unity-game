﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorScript : MonoBehaviour {
    public GameObject key;
    //Private Variables
    GameObject robot;
    GameObject frontCollider;
    GameObject backCollider;
    CharControler charControler;
    // Use this for initialization
    void Start () {
        robot = GameObject.FindGameObjectWithTag("Player");
        charControler = robot.GetComponent<CharControler>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider c)
    {
        bool pass = false;
        
        foreach (Transform child in robot.transform)
        {
            //print(child);
            if (child.gameObject == key)
            {
                //print("Found Key!");
                pass = true;
            }
        }
        
        if (!pass) { 
            if (c.gameObject.tag == "FrontCollider")
            {
                print("Front Colide");
                charControler.frontFree = false;
            }
            if (c.gameObject.tag == "BackCollider")
            {
                print("Back Colide");
                charControler.backFree = false;
            }
            print("You shall not pass!");
        }
        //print("Something enter");
       
    }

    void OnTriggerExit(Collider c)
    {
        //print("Something exit");
        if (c.gameObject.tag == "FrontCollider")
        {
            print("Front Free");
            charControler.frontFree = true;
        }
        if (c.gameObject.tag == "BackCollider")
        {
            print("Back Free");
            charControler.backFree = true;
        }
    }

}
