﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class travelator_move : MonoBehaviour {

	public GameObject robot;
	float moveSpeed=0.4f;
	public Vector3 direction;

	void OnCollisionStay(Collision c)
	{
		if (!robot.GetComponent<CharControler> ().moving) {
			move();	
		}
	}

	void OnCollisionEnter (Collision c){
		if (c.gameObject == robot) {
			robot = GameObject.FindGameObjectWithTag ("Player");
		}
	}

	void move() {
		FindObjectOfType<AudioManager> ().PlaySound ("RoboMove");
		StartCoroutine (go (-1));
	}

	IEnumerator go(int i) {
			Vector3 movement = Vector3.Normalize (direction) * (moveSpeed*2) * Time.deltaTime * i;
		Vector3 destine = robot.transform.position + Vector3.Normalize(direction) * i*0.5F;
			Vector3 start = robot.transform.position;
			float toTravle = Vector3.Distance (destine, start);
			float travelled = 0;
			FindObjectOfType<AudioManager> ().PlaySound ("RoboCharge");

			while (travelled < toTravle) {
				robot.transform.position += movement;
				travelled = Vector3.Distance (robot.transform.position, start);
				yield return null;
			}
			robot.transform.position = destine;
	}
}


