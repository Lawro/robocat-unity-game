﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TronLight : MonoBehaviour {
    bool robotInRange;
    GameObject robot;
    Light l;
    void Start () {
        robot = GameObject.FindGameObjectWithTag("Player");
        l = transform.GetComponent<Light>();
    }
	
	// Update is called once per frame
	void Update () {
        if (robotInRange)
        {
            print("ColidedLight");
        }
    }

    void OnTriggerEnter(Collider c)
    {
        
        if (c.gameObject == robot)
        {
            robotInRange = true;
            l.intensity = 1.5f;
        }
    }

    void OnTriggerExit(Collider c)
    {
        if (c.gameObject == robot)
        {
            robotInRange = false;
            l.intensity = 0.5f;
        }
    }
}
