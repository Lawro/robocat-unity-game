﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CompilerError : MonoBehaviour {

	GameObject sidebar = null;
	GameObject compileErr = null;

	// Use this for initialization
	void Start () {
		Transform Ui = transform.root.Find ("UI");
		if (Ui == null) {
			throw new Exception ("Failed to get UI");
		}
		sidebar = Ui.Find ("Sidebar").gameObject;
		if (sidebar == null) {
			throw new Exception ("Failed to get sidebar");
		}
		compileErr = Ui.Find ("CompilerError").gameObject;
		if (compileErr == null) {
			throw new Exception ("Failed to get compiler error");
		}
		compileErr.SetActive (false);
	}

	public void showError(string err) {
		sidebar.SetActive (false);
		compileErr.SetActive (true);
		Transform panel = compileErr.transform.Find ("Panel");
		if (panel == null) {
			throw new Exception ("Failed to get panel");
		} else {
			Text text = panel.Find("Text").GetComponent<Text> ();
			text.text = "Error in your code! " + err;
		}
	}

}
