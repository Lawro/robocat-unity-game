﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class alterscene : MonoBehaviour {

	public void ChangetoScene (int sceneToChangeTo)
    {
        SceneManager.LoadScene(sceneToChangeTo);
    }
}
