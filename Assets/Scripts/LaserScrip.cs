﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScrip : MonoBehaviour {
    LineRenderer laser;
	// Use this for initialization
	void Start () {
        laser = gameObject.GetComponent<LineRenderer>();
        laser.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Q))
        {
            print("Q pressed");
            laserOn();
        }
	}

    void laserOn()
    {
        laser.enabled = true;
        Ray ray = new Ray(transform.position, transform.forward);

        laser.SetPosition(0, ray.GetPoint(-20));
        laser.SetPosition(1, ray.GetPoint(20));
        print("Q pressed2");
        
    }


}
