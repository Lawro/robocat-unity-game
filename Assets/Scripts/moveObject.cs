﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveObject : MonoBehaviour {


    public GameObject item;
    public GameObject tempParent;
    public Transform guide;

   


	// Use this for initialization
	void Start () {
        item.GetComponent<Rigidbody>().useGravity = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        item.GetComponent<Rigidbody>().useGravity = false;
        item.GetComponent<Rigidbody>().isKinematic = true;
        item.transform.position = new Vector3(guide.transform.position.x, guide.transform.position.y, guide.transform.position.z-1);
        item.transform.parent = tempParent.transform;

    }
    private void OnMouseUp()
    {
        item.GetComponent<Rigidbody>().useGravity = true;
        item.GetComponent<Rigidbody>().isKinematic = false;
        item.transform.parent = null;
        item.transform.position = new Vector3(guide.transform.position.x, guide.transform.position.y, guide.transform.position.z - 1);
    }
}
