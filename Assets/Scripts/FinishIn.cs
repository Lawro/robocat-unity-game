﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishIn : MonoBehaviour {
    //Private Variables
    GameObject robot;
    CharControler charControler;

    // Use this for initialization
    void Start () {
        robot = GameObject.FindGameObjectWithTag("Player");
        charControler = robot.GetComponent<CharControler>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject == robot)
        {
            //End level
            finishLevel();
        }
    }

    void OnCollisionExit(Collision c)
    {
        if (c.gameObject == robot)
        {
            //This is not supposed to happen
        }
    }

    public void finishLevel()
    {
        //charControler.frontFree = false;//Only to make the robot stop
        print("Level finished!!");
    }

}
