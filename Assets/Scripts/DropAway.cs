﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropAway : MonoBehaviour {



    public GameObject robot;
    public GameObject dropTile;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionExit(Collision collision)
    {
        if(collision.gameObject == robot)
        {
            dropTile.SetActive(false);
        }
    }

}
