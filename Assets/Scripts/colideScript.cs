﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colideScript : MonoBehaviour {
    //Private Variables
    GameObject robot;
    GameObject frontCollider;
    GameObject backCollider;
    CharControler charControler;

    // Use this for initialization
    void Start () {
        robot = GameObject.FindGameObjectWithTag("Player");
        charControler = robot.GetComponent<CharControler>();
        frontCollider = GameObject.FindGameObjectWithTag("FrontCollider");
        backCollider = GameObject.FindGameObjectWithTag("BackCollider");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider c)
    {
        
        //print("Something enter");
        if (c.gameObject.tag == "FrontCollider")
        {
            print("Front Colide");
            charControler.frontFree = false;
        }
        if (c.gameObject.tag == "BackCollider")
        {
            print("Back Colide");
            charControler.backFree = false;
        }
        
    }

    void OnTriggerExit(Collider c)
    {
        
        //print("Something exit");
        if (c.gameObject.tag == "FrontCollider")
        {
            print("Front Free");
            charControler.frontFree = true;
        }
        if (c.gameObject.tag == "BackCollider")
        {
            print("Back Free");
            charControler.backFree = true;
        }
        
    }
}
