﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalIn : MonoBehaviour {
    //Public Variables
    public GameObject PortalOut;
    //Private Variables
    GameObject robot;
    CharControler charControler;

    // Use this for initialization
    void Start () {
        robot = GameObject.FindGameObjectWithTag("Player");
        charControler = robot.GetComponent<CharControler>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject == robot)
        {
            Vector3 destine = new Vector3();
            destine.z = PortalOut.transform.position.z;
            destine.x = PortalOut.transform.position.x;
            destine.y = robot.transform.position.y;
            print(destine.x);
            charControler.teleport(destine);
        }
    }

    void OnCollisionExit(Collision c)
    {
        if (c.gameObject == robot)
        {
            //Nothing happens - if you are reading this congrats!
        }
    }

}
