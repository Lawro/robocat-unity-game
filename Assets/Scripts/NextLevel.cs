﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NextLevel : MonoBehaviour {

	GameObject NextLevelPanel;
	GameObject robot;
	GameObject GameOverMenu;
	GameObject sidebar;

	// Use this for initialization
	void Start () {
		Transform Ui = transform.root.Find ("UI");
		if (Ui == null) {
			throw new Exception ("Failed to get UI");
		}
		NextLevelPanel = Ui.Find ("youWinMenu").gameObject;
		if (NextLevelPanel == null) {
			throw new Exception ("Failed to get next level panel");
		}
		NextLevelPanel.SetActive (false);
		GameOverMenu = Ui.Find ("GameOverMenu").gameObject;
		if (GameOverMenu == null) {
			throw new Exception ("Failed to get game over menu");
		}
		sidebar = Ui.Find ("Sidebar").gameObject;
		if (sidebar == null) {
			throw new Exception ("Failed to get sidebar");
		}
		Transform robotPrefab = transform.root.Find ("RobotPrefab");
		if (robotPrefab == null) {
			throw new Exception ("Failed to get Robot Prefab");
		}
		robot = robotPrefab.Find ("Robot").gameObject;
		if (robot == null) {
			throw new Exception ("Failed to get robot");
		}
		Transform tutorial = Ui.Find ("Tutorial");
		if (tutorial != null) {
			sidebar.SetActive (false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		
    private void OnCollisionEnter(Collision collision)
    {
		if (robot.name.Equals(collision.gameObject.name)) {
			CharControler c = robot.GetComponent<CharControler> ();
			if (c == null) {
				throw new Exception ("Failed to get char controller");
			} else {
				c.won = true;
			}
			NextLevelPanel.SetActive (true);
			FindObjectOfType<AudioManager> ().PlaySound ("Win");
			GameOverMenu.SetActive (false);
			sidebar.SetActive (false);
		}
    }

}
