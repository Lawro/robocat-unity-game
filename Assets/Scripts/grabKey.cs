﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grabKey : MonoBehaviour {
    public GameObject key;
    public float yOffset;
    //Private Variables
    GameObject robot;
    private bool follow;
    // Use this for initialization
    void Start () {
        robot = GameObject.FindGameObjectWithTag("Player");
    }
	
	// Update is called once per frame
	void Update () {
        /*if (follow)
        {
            //Do nothing print("Following");
        }*/
		
	}

    void OnTriggerEnter(Collider c) 
    {
        if (c.gameObject.tag == "body")
        {
            follow = true;
            key.transform.parent = robot.transform;
            key.transform.position = new Vector3(robot.transform.position.x, robot.transform.position.y- yOffset, robot.transform.position.z);
        }
    }

    void OnCollisionExit(Collision c)
    {
        if (c.gameObject == robot)
        {
            //Nothing happens - if you are reading this congrats!
        }
    }
}
