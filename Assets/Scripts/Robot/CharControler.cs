﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharControler : MonoBehaviour
{
    [SerializeField]
    public float moveSpeed = 0.4f;
    public float moveTime = 5f;
    public float turnSpeed = 10f;
    public float turnTime = 10f;
    public int batteryConsumption = 10;
	public GameObject UIPanel;
    public GameObject demoSkip;
	public bool won = false;
    // Private variables
	private bool stepback = false;
    float moveSize = 7.5f;
    int turnAngle = 90;
    public bool spinning = false;
    public bool moving = false;
    GameObject robot;
    RobotBattery robotBattery;
    Vector3 forward, right;
    public bool frontFree = true;
    public bool backFree = true;

    // Use this for initialization
    void Start()
    {
        
       
        robot = GameObject.FindGameObjectWithTag("Player");
        robotBattery = robot.GetComponent<RobotBattery>();
        forward = Vector3.forward;//Camera.main.transform.forward;
        forward.y = 0;
        forward = Vector3.Normalize(forward);
        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;
    }

    // Update is called once per frame
    void Update()
    {
		if (!won) {
			if (Input.GetKeyUp(KeyCode.J)) { TurnLeft(); }
			if (Input.GetKeyUp(KeyCode.L)) { TurnRight(); }
			if (Input.GetKeyUp(KeyCode.I)) { goForward(); }
			if (Input.GetKeyUp(KeyCode.K)) { goBackward(); }
			if (Input.GetKeyUp(KeyCode.H)) { jumpForward(); }
			if (Input.GetKeyUp(KeyCode.T) && !spinning)
				StartCoroutine(Spin(1));
			if (Input.GetKeyUp (KeyCode.M)) {
				demoSkip.SetActive(true);
				UIPanel.SetActive(false);
			}
            if (Input.GetKeyUp(KeyCode.S)) { MainMenu(); }
            if (Input.GetKeyUp(KeyCode.R)) { reloadLevel(); }

			/*if(Input.anyKey){
            Move();
        }*/
			if (transform.position.y < 0.8) robotBattery.FallDeath(); //To make sure the robot cant move after falling
		}
    }

    //Move Cube
    void Move()
    {
        float horizontal = Input.GetAxis("HorizontalKey");
        float vertical = Input.GetAxis("VerticalKey");
        float jump = 0f;
        Vector3 direction = new Vector3(horizontal, jump, vertical);
        Vector3 rightMovement = right * moveSpeed * Time.deltaTime * horizontal;
        Vector3 upMovement = forward * moveSpeed * Time.deltaTime * vertical;
        Vector3 heading = -Vector3.Normalize(rightMovement + upMovement);

        transform.forward = heading;
        transform.position += rightMovement;
        transform.position += upMovement;
    }
    //Teleporting character
    public void teleport(Vector3 destine) {
        print("Teleporting");
        transform.position = destine;
    }
		
	IEnumerator jump(int i)
	{
		robot.GetComponent<Rigidbody>().useGravity = false;
		moving = true;
		Vector3 movementDown = Vector3.Normalize(transform.up) * (moveSpeed * 3)* Time.deltaTime * i;
		Vector3 movementUp = -Vector3.Normalize(transform.up) * (moveSpeed * 3)* Time.deltaTime * i;
		Vector3 movement = Vector3.Normalize (transform.forward) * (moveSpeed * 3) * Time.deltaTime * i;
		Vector3 movementBack = (-1)*Vector3.Normalize (transform.forward) * (moveSpeed * 3) * Time.deltaTime * i;
		//Commented out the moveSize multiplier as grid is now in UI space
		Vector3 destine = transform.position + 2*Vector3.Normalize(transform.forward) * /*moveSize */ i;
		Vector3 start = transform.position;
		float toTravle = Vector3.Distance(destine, start);
		float travelled = 0;
		float lift=0;
		float catHeight = robot.GetComponent<BoxCollider>().size.y/4;
		bool flag=true;


		while ((travelled<toTravle || (lift>=0)) && (!stepback))
		{
			if (travelled==0 && (lift<catHeight)) {
				transform.position += movementUp;
				lift = transform.position.y - start.y;
				yield return null;
			}
			else if (travelled<toTravle){
				transform.position += movement;
				Vector3 temp=new Vector3(transform.position.x,start.y,transform.position.z);
				travelled = Vector3.Distance (temp, start);
				yield return null;
			}
			else {
				transform.position += movementDown;
				lift = transform.position.y - start.y;
				yield return null;
			}
		}
		while ((travelled < toTravle || (lift >= 0)) && (stepback)) {
			//print ("Right loop");
			if (travelled >= toTravle && flag) {
				travelled = 0;
				toTravle = toTravle / 2;
				flag = !flag;
				yield return null;
			} else if (travelled < toTravle) {
				transform.position += movementBack;
				Vector3 temp = new Vector3 (transform.position.x, start.y, transform.position.z);
				travelled = Vector3.Distance (temp, destine);
				yield return null;
			} else {
				transform.position += movementDown;
				lift = transform.position.y - start.y;
				yield return null;
			}
		}
		if (!stepback) {
			transform.position = destine;
			robot.GetComponent<Rigidbody> ().useGravity = true;
			moving = false;
		}
		else {
			destine = start + Vector3.Normalize(transform.forward) * /*moveSize */ i;
			transform.position = destine;
			stepback = !stepback;
			robot.GetComponent<Rigidbody> ().useGravity = true;
			moving = false;
		}
	}

	void OnCollisionEnter(Collision c)
	{	
		if (c.gameObject.tag=="Wall")
		{
			stepback = !stepback;
		}
	}

	/*.void OnTriggerEnter(Collider c)
	{	
		if (c.gameObject.tag=="Wall")
		{
			stepback = !stepback;
		}
	} */
 
	public void jumpForward()
	{
		if (!spinning && !moving && robotBattery.currentBattery > 0)
		{
			FindObjectOfType<AudioManager>().PlaySound ("RoboMove");
			StartCoroutine(jump(-1));
			drainBattery();
			drainBattery();
		}
	}

    //Move character
    IEnumerator go(int i)
    {
			moving = true;
			Vector3 movement = Vector3.Normalize(transform.forward) * moveSpeed * Time.deltaTime * i;
			//Commented out the moveSize multiplier as grid is now in UI space
			Vector3 destine = transform.position + Vector3.Normalize(transform.forward) * /*moveSize */ i;
			Vector3 start = transform.position;
			float toTravle = Vector3.Distance(destine, start);
			float travelled = 0;

			while (travelled < toTravle)
			{
				transform.position += movement;
				travelled = Vector3.Distance(transform.position, start);
				yield return null;
			}
			transform.position = destine;
			moving = false;
    }

    public void goForward()
    {
		if (!won) {
			if (!spinning && !moving && robotBattery.currentBattery > 0 && frontFree) {
				FindObjectOfType<AudioManager> ().PlaySound ("RoboMove");
				StartCoroutine (go (-1));
				drainBattery ();
			}
		}
    }

    void goBackward()
    {
		if (!won) {
			if (!spinning && !moving && robotBattery.currentBattery > 0 && backFree) {
				FindObjectOfType<AudioManager> ().PlaySound ("RoboMove");
				StartCoroutine (go (1));
				drainBattery ();
			}
		}
    }

    //TUrn character
    void Turn(int i)
    {
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + (i * turnAngle), 0);
    }

    public void TurnLeft()
    {
		if (!won) {
			if (!spinning && !moving && robotBattery.currentBattery > 0) {
				FindObjectOfType<AudioManager> ().PlaySound ("RoboTurn");
				StartCoroutine (Spin (-1));
				drainBattery ();
			}
		}
    }

    public void TurnRight()
    {
		if (!won) {
			if (!spinning && !moving && robotBattery.currentBattery > 0) {
				FindObjectOfType<AudioManager> ().PlaySound ("RoboTurn");
				StartCoroutine (Spin (1));
				drainBattery ();
			}
		}
    }

    IEnumerator Spin(int i)
    {
        spinning = true;
        var turnIncrement = new Vector3(0, i * turnSpeed * 2 * Time.deltaTime * 10, 0);
        Vector3 goal = new Vector3(0, (transform.eulerAngles.y + (i * turnAngle)), 0);
        var curFwd = transform.forward;
        var ang = Vector3.Angle(curFwd, transform.forward);

        while (ang < 85)
        {
            transform.Rotate(turnIncrement);
            ang = Vector3.Angle(curFwd, transform.forward);
            yield return null;
        }

        transform.eulerAngles = goal;
        spinning = false;
    }

    void drainBattery()
    {
        if (robotBattery.currentBattery >= 0)
        {
            robotBattery.drainBattery(batteryConsumption);
        }
    }

    void MainMenu()
    {
        SceneManager.LoadScene("Intro");
    }

    void reloadLevel()
    {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(sceneIndex);
    }
}
