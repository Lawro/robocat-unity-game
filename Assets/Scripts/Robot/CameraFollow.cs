﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;
    public float smoothing = 5f;
    public Camera cam;
    //Private
    float minFov = 5f;
    float maxFov = 15f;
    float sensitivity = 10f;
	float duration = 5f;
	static float startTime;
    Vector3 offset;

	void Start()
	{
		cam = GetComponent<Camera>();
		offset = transform.position - target.position;
		cam.orthographicSize = 7;
		startTime = Time.time;
	}

	void Update()
	{
		float fov = cam.orthographicSize;
		//print(fov);
		//float t = (Time.time - startTime) / duration;
		//cam.orthographicSize = Mathf.SmoothStep(maxFov, minFov, t);
		fov += Input.GetAxis("Mouse ScrollWheel")* sensitivity;
		fov = Mathf.Clamp(fov, minFov, maxFov);
		cam.orthographicSize = fov;
	}

    void FixedUpdate()
    {
        Vector3 targetCamPos = target.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }

}
