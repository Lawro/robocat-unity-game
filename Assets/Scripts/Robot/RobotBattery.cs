﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class RobotBattery : MonoBehaviour
{
    //Public variables
    public int startBattery = 100;
    public int currentBattery;
    public Slider batterySlider;
    public GameObject GameOverMenu;
	public GameObject FallDeathMenu;
	public GameObject sidebar;

    //Private variables
    CharControler charControler;
    bool isDead;
    bool draining;
    // Use this for initialization
    void Start()
    {
        charControler = GetComponent<CharControler>();
        currentBattery = startBattery;
		Transform Ui = transform.root.Find ("UI");
		if (Ui == null) {
			throw new Exception("failed to get UI");
		} else {
			FallDeathMenu = Ui.Find ("FallDeathMenu").gameObject;
			if (FallDeathMenu == null) {
				throw new Exception ("failed to get Fall Death Menu");
			} else {
				FallDeathMenu.SetActive (false);
			}
		}
    }

    // Update is called once per frame
    void Update()
    {
        if (draining)
        {
            //print("battery draining...");
        }
        draining = false;
    }

    public void drainBattery(int amount)
    {
        draining = true;
        currentBattery -= amount;
        batterySlider.value = currentBattery;
        if (currentBattery <= 0 && !isDead)
        {
            BatteryDeath();
        }
       /* if (currentBattery == 0)
        {
            //WaitForCharge();
            if (currentBattery == 0) {
                throwpopup();
            }
        }*/
        //print(currentBattery);
    }

    public IEnumerator WaitForCharge()
    {
        yield return new WaitForSecondsRealtime(40);
    }

    public void BatteryDeath()
    {
		if (!charControler.won) {
			currentBattery = -10;
			batterySlider.value = currentBattery;
			isDead = true;
			charControler.enabled = false;
			GameOverMenu.SetActive (true);
			FindObjectOfType<AudioManager> ().PlaySound ("Lose1");
			FindObjectOfType<AudioManager> ().PlaySound ("Lose2");
			sidebar.SetActive (false);
		}
    }

	public void CompileDeath()
	{
		if (!charControler.won) {
			currentBattery = -10;
			batterySlider.value = currentBattery;
			isDead = true;
			charControler.enabled = false;
			GameOverMenu.SetActive (true);
			Transform panel = GameOverMenu.transform.Find ("Panel");
			if (panel == null) {
				throw new Exception ("Failed to get panel");
			} else {
				FindObjectOfType<AudioManager> ().PlaySound ("Lose1");
				FindObjectOfType<AudioManager> ().PlaySound ("Lose2");
				Text text = panel.Find ("Text").GetComponent<Text> ();
				text.text = "Oh no! You hit your compile limit. Better luck next time!";
			}
			sidebar.SetActive (false);
		}
	}


	public void FallDeath()
	{
		if (!charControler.won) {
			currentBattery = -10;
			batterySlider.value = currentBattery;
			isDead = true;
			charControler.enabled = false;
			FallDeathMenu.SetActive (true);
			sidebar.SetActive (false);
			FindObjectOfType<AudioManager> ().PlaySound ("Lose1");
			FindObjectOfType<AudioManager> ().PlaySound ("Lose2");
			GameObject batSlide = GameObject.Find ("BatteryCanvas");
			batSlide.SetActive (false);
            GameObject.Find("GridCamera").GetComponent<CameraFollow>().enabled = false;

		}
	}
		
}
