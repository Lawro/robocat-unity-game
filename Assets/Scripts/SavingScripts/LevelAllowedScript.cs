﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelAllowedScript : MonoBehaviour {


    public GameObject DeCharge;
    public GameObject Travelator;
    public GameObject DoorandKey;
    public GameObject Challenge;
	// Use this for initialization
	void Start () {
        if (PersistentManager.Instance.Value.Contains(3))
        {
            DeCharge.SetActive(true);
        }
        else
        {
            DeCharge.SetActive(false);
        }

        if (PersistentManager.Instance.Value.Contains(4))
        {
            Travelator.SetActive(true);
        }
        else
        {
            Travelator.SetActive(false);
        }

        if (PersistentManager.Instance.Value.Contains(5))
        {
            DoorandKey.SetActive(true);
        }
        else
        {
            DoorandKey.SetActive(false);
        }

        if (PersistentManager.Instance.Value.Contains(6))
        {
            Challenge.SetActive(true);
        }
        else
        {
            Challenge.SetActive(false);
        }


    }

    // Update is called once per frame
    void Update () {
		
	}
}
