﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RechargeScript : MonoBehaviour
{
    //Public attributes
    public int rechargeAmount;
    public float timeBetweenCharges;
    public GameObject GameOverMenu;
    public ParticleSystem particles;
    //Private attributes
    float timer;
    bool robotInRange;
    GameObject robot;
    RobotBattery robotBattery;
    
    // Use this for initialization
    void Start()
    {
        timer = 0f;
        robot = GameObject.FindGameObjectWithTag("Player");
        robotBattery = robot.GetComponent<RobotBattery>();
        particles.Pause();
    }

    // Update is called once per frame
    void Update()
    {
        timer += (Time.deltaTime);
        if (robotInRange && timer >= timeBetweenCharges)
        {
            recharge();
        }
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject == robot)
        {
            robotInRange = true;
        }
    }

    void OnCollisionExit(Collision c)
    {
        if (c.gameObject == robot)
        {
            robotInRange = false;
            particles.Stop();
        }
    }

    void recharge()
    {
        if (robotBattery.currentBattery < 100)
        {
            particles.Play();
            if (this.name == "DischargeSquare") {
				FindObjectOfType<AudioManager> ().PlaySound ("RoboDrain");
                robotBattery.drainBattery(-rechargeAmount);
                timer = 0f;
            } else if (this.name == "RechargeSquare") {
				FindObjectOfType<AudioManager> ().PlaySound ("RoboCharge");
                //Changed -RechargeAmount to -10, @Henrique Changed back
                robotBattery.drainBattery(-rechargeAmount);
                timer = 0f;
			}
        }
        else
        {
            particles.Stop();
        }
    }
    

}
