﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MoveBack : MonoBehaviour {
	
	public string desc = "Move the robot back one square";

	public void execute(int delay) {
		GameObject roboP = GameObject.Find ("RobotPrefab");
		Transform robPref = roboP.transform;
		if (robPref == null) {
			throw new Exception ("Failed to get robot prefab");
		} else {
			Transform robot = robPref.Find ("Robot");
			if (robot == null) {
				throw new Exception ("Failed to get robot");
			} else {
				CharControler cont = robot.GetComponent<CharControler> ();
				if (cont == null) {
					throw new Exception ("Failed to get CharControler");
				} else {
					//cont.goForward ();
					cont.Invoke("goBackward", delay);
				}
			}
		}
	}


}
