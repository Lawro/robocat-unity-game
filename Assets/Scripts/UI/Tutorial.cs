﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Tutorial : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Transform Ui = transform.root.Find ("UI");
		if (Ui == null) {
			throw new Exception ("Failed to get UI");
		}
		Transform tutorial = Ui.Find ("Tutorial");
		if (tutorial != null) {
			GameObject sidebar = Ui.Find ("Sidebar").gameObject;
			if (sidebar == null) {
				throw new Exception ("Failed to get sidebar");
			}
			sidebar.SetActive (false);
		}
	}

}
