using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Turn180Deg : MonoBehaviour {

	public string desc = "Rotate the robot a full 180 degrees";

	public void execute(int delay) {
		GameObject roboP = GameObject.Find ("RobotPrefab");
		//Transform robPref = transform.root.Find ("RobotPrefab");
		Transform robPref = roboP.transform;
		if (robPref == null) {
			throw new Exception ("Failed to get robot prefab");
		} else {
			Transform robot = robPref.Find ("Robot");
			if (robot == null) {
				throw new Exception ("Failed to get robot");
			} else {
				CharControler cont = robot.GetComponent<CharControler> ();
				if (cont == null) {
					throw new Exception ("Failed to get CharControler");
				} else {
					//cont.TurnRight ();
					cont.Invoke("TurnRight", delay);
					//Wait 1 second, turn right again
					cont.Invoke("TurnRight", delay+1);
				}
			}
		}
	}
}

