﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Text;

public class UiTile : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler  {

	public Transform endPoint;
	public string functionName;
	public int remInstantiations;
	private Color32 DISABLED;
	private Color32 ENABLED;

	// Initialise colour constants
	void Start() {
		DISABLED = new Color32 (131, 127, 108, 255);
		ENABLED = new Color32 (255, 206, 0, 255);
	}

	// Check if a tooltip is warranted on mouse-over and display one if so
	public void OnPointerEnter(PointerEventData eventData) {
		if (transform.parent.name.Equals ("Dragzone") || transform.parent.name.Equals ("Dropzone")) {
			displayTooltip ();
		}
	}

	// Display a tooltip to describe a tile's associated action
	private void displayTooltip() {
		string msg = "";
		Move move = gameObject.GetComponent<Move> ();
		if (move != null) {
			msg = msg + move.desc + ". ";
		}
		Turn90Deg t90 = gameObject.GetComponent<Turn90Deg> ();
		if (t90 != null) {
			msg = msg + t90.desc + ". ";
		}
		TurnMinus90Deg tm90 = gameObject.GetComponent<TurnMinus90Deg> ();
		if (tm90 != null) {
			msg = msg + tm90 .desc + ". ";
		}
		Turn180Deg t180 = gameObject.GetComponent<Turn180Deg> ();
		if (t180 != null) {
			msg = msg + t180.desc + ". ";
		}
		MoveBack mb = gameObject.GetComponent<MoveBack> ();
		if (mb != null) {
			msg = msg + mb.desc + ". ";
		}
		Jump j = gameObject.GetComponent<Jump> ();
		if (j != null) {
			msg = msg + j.desc + ". ";
		}
		ForStart fs = gameObject.GetComponent<ForStart> ();
		if (fs != null) {
			msg = msg + fs.desc + ". ";
		}
		ForEnd fe = gameObject.GetComponent<ForEnd> ();
		if (fe != null) {
			msg = msg + fe.desc + ". ";
		}

		GameObject tip = (GameObject)Instantiate (Resources.Load ("Tip"));
		if (tip == null) {
			throw new Exception("Failed to get tip object prefab");
		} else {
			tip.name = "Tip";
			Text text = tip.transform.Find("Text").GetComponent<Text> ();
			text.text = msg;
			tip.transform.SetParent (transform.parent.parent, false);
		}		
	}

	// Erase the tooltip on mouse-exit
	public void OnPointerExit(PointerEventData eventData) {
		destroyTooltip ();
	}

	// Get rid of the 'tip' object if it exists
	private void destroyTooltip() {
		Transform Ui = transform.root.Find ("UI");
		if (Ui == null) {
			throw new Exception ("Failed to get Ui");
		} else {
			Transform MainWindow = Ui.Find ("MainWindow");
			if (MainWindow == null) {
				throw new Exception ("Failed to get main window");
			} else {
				Transform Panel = MainWindow.Find ("Panel");
				if (Panel == null) {
					throw new Exception ("Failed to get panel");
				} else {
					Transform tip = Panel.Find ("Tip");
					if (tip != null) {
						Destroy (tip.gameObject);
					}
				}
			}
		}
	}
		
	// Respond to the a tile starting to be dragged
	public void OnBeginDrag(PointerEventData eventData) {
		if (remInstantiations > 0) {
			if ("Dragzone".Equals (transform.parent.name)) {
				replaceTile ();
			} else if ("Dropzone".Equals (transform.parent.name)) {
				Dropzone d = transform.parent.GetComponent<Dropzone> ();
				if (d == null) {
					throw new Exception("Failed to get dropzone component");
				} else {
					d.tiles--;
					endPoint = transform.parent;
				}	
			}
			setTextWoLimit ();
			transform.SetParent (transform.parent.parent.parent.Find("Panel"));
			GetComponent<CanvasGroup>().blocksRaycasts = false;
		}
	}

	// Replicate current tile and stick copy in the dragzone
	private void replaceTile() {
		GameObject replacement = (GameObject)Instantiate (Resources.Load ("UiTile"));
		if (replacement == null) {
			throw new Exception ("Failed to get UI tile object prefab");
		} else {
			replacement.transform.SetParent (transform.parent, false);
			replacement.transform.SetSiblingIndex (transform.GetSiblingIndex ());
			copyScripts (replacement);
			Text text = replacement.transform.Find("Text").GetComponent<Text> ();
			text.text = functionName;
			UiTile ui = replacement.GetComponent<UiTile> ();
			if (ui == null) {
				throw new Exception ("Failed to get UI tile component");
			} else {
				ui.remInstantiations = remInstantiations - 1;
				if (ui.remInstantiations == 0) {
					replacement.GetComponent<Image> ().color = DISABLED;
				}
				ui.functionName = functionName;
				ui.setTextWLimit ();
			}
		}
	}

	// Copy all attached scripts from this tile to a game object passed as argument
	public void copyScripts(GameObject replacement) {
		if (gameObject.GetComponent<Move> () != null) {
			replacement.AddComponent<Move> ();
		}
		if (gameObject.GetComponent<Turn90Deg> () != null) {
			replacement.AddComponent<Turn90Deg> ();
		}
		if (gameObject.GetComponent<TurnMinus90Deg> () != null) {
			replacement.AddComponent<TurnMinus90Deg> ();
		}
		if (gameObject.GetComponent<Turn180Deg> () != null) {
			replacement.AddComponent<Turn180Deg> ();
		}
		if (gameObject.GetComponent<MoveBack> () != null) {
			replacement.AddComponent<MoveBack> ();
		}
		if (gameObject.GetComponent<Jump> () != null) {
			replacement.AddComponent<Jump> ();
		}
		ForStart oldF = gameObject.GetComponent<ForStart> ();
		if (oldF != null) {
			replacement.AddComponent<ForStart> ();
			ForStart newF = replacement.GetComponent<ForStart> ();
			newF.iterations = oldF.iterations;
		}
		if (gameObject.GetComponent<ForEnd> () != null) {
			replacement.AddComponent<ForEnd> ();
		}
	}

	// Make the tile follow the mouse that is dragging it
	// Move placeholder to follow tile
	public void OnDrag(PointerEventData eventData)
	{
		if(remInstantiations > 0 && transform.parent.name != "Commands") {
			transform.position = eventData.position;
			adjustPlaceholder ();
		}
	}

	// Calculate where the placeholder should be and move it if necessary
	private void adjustPlaceholder() {
		Dropzone d = getDropzoneComponent();
		if (d.placeholder != null) {
			d.placeholder.transform.SetSiblingIndex (d.placeholder.transform.parent.childCount);
			for (int i = 0; i < d.placeholder.transform.parent.childCount; i++) {
				if (transform.position.y > d.placeholder.transform.parent.GetChild (i).position.y) {
					d.placeholder.transform.SetSiblingIndex (i);
					return;
				}
			}
		}
	}
		
	private Transform getDropzone() {
		Transform Ui = transform.root.Find ("UI");
		if (Ui == null) {
			throw new Exception ("Failed to get Ui");
		} else {
			Transform MainWindow = Ui.Find ("MainWindow");
			if (MainWindow != null) {
				Transform Panel = MainWindow.Find ("Panel");
				if (Panel != null) {
					Transform d = Panel.Find ("Dropzone");
					if (d != null) {
						return d;
					}
				}
			}
		}
		throw new Exception ("Failed to get dropzone transform");
		return null;
	}
		
	// Get the 'Dropzone' script attached to the 'Dropzone' transform
	private Dropzone getDropzoneComponent() {
		Transform dt = getDropzone ();
		if (dt != null) {
			Dropzone d = dt.GetComponent<Dropzone> ();
			if (d != null) {
				return d;
			}
		}
		throw new Exception ("Failed to get dropzone component");
		return null;
	}

	// Calculate where the tile should be placed
	// Destroy placeholder if there was one
	public void OnEndDrag(PointerEventData eventData)
	{
		Dropzone d = getDropzoneComponent ();
		if (remInstantiations > 0) {
			GetComponent<CanvasGroup> ().blocksRaycasts = true;
			if (endPoint == null || "Dropzone".Equals (endPoint.name)) {
				add2Dropzone ();
			}
		}
		if (d.placeholder != null) {
			Destroy (d.placeholder);
		}
	}

	// Make the current tile a child of the Dropzone
	private void add2Dropzone() {
		Dropzone d = getDropzoneComponent ();
		if (d != null) {
			if (d.tiles + 1 > d.TILE_LIMIT) {
				Dragzone dg = getDragzoneComponent ();
				if (dg != null) {
					dg.increment(functionName);
				}
				Destroy (gameObject);
			} else {
				Transform dt = getDropzone ();
				if (dt != null) {
					transform.SetParent (dt);
					d.tiles++;
					if (d.placeholder != null) {
						transform.SetSiblingIndex (d.placeholder.transform.GetSiblingIndex ());
					}
				}
			}	
		}
	}

	// Get the 'Dragzone' script attached to the 'Dragzone' game object
	private Dragzone getDragzoneComponent() {
		Transform Ui = transform.root.Find ("UI");
		if (Ui == null) {
			throw new Exception ("Failed to get Ui");
		} else {
			Transform MainWindow = Ui.Find ("MainWindow");
			if (MainWindow != null) {
				Transform Panel = MainWindow.Find ("Panel");
				if (Panel != null) {
					Transform dt = Panel.Find ("Dragzone");
					if (dt != null) {
						Dragzone d = dt.GetComponent<Dragzone> ();
						if (d != null) {
							return d;
						}
					}
				}
			}
		}
		throw new Exception ("Failed to get Dragzone component");
		return null;
	}
		
	// Call the actions associated with scripts attached to this tile
	public void execute(int delay) {
		Move move = gameObject.GetComponent<Move> ();
		if (move != null) {
			move.execute(delay);
		}
		Turn90Deg t90 = gameObject.GetComponent<Turn90Deg> ();
		if (t90 != null) {
			t90.execute(delay);
		}
		TurnMinus90Deg tm90 = gameObject.GetComponent<TurnMinus90Deg> ();
		if (tm90 != null) {
			tm90.execute(delay);
		}
		Turn180Deg t180 = gameObject.GetComponent<Turn180Deg> ();
		if (t180 != null) {
			t180.execute(delay);
		}
		MoveBack mb = gameObject.GetComponent<MoveBack> ();
		if (mb != null) {
			mb.execute (delay);
		}
		Jump j = gameObject.GetComponent<Jump> ();
		if (j != null) {
			j.execute (delay);
		}
	}

	// Set tile colour to an 'enabled' orange
	public void enable() {
		GetComponent<Image> ().color = ENABLED;
	}
		
	// Set tile text to the function name only
	public void setTextWoLimit() {
		if ("for(int i = 0; i < ?; i++) {".Equals (functionName)) {
			ForStart fs = gameObject.GetComponent<ForStart> ();
			StringBuilder sb = new StringBuilder (functionName);
			sb [functionName.IndexOf ('?')] = (char)((char)fs.iterations + '0');
			string newText = sb.ToString ();
			Text text = transform.Find ("Text").GetComponent<Text> ();
			text.text = newText;
		} else {
			Text text = transform.Find("Text").GetComponent<Text> ();
			text.text = functionName;
		}
	}

	// Set tile text to the function name and tile limit number
	public void setTextWLimit() {
		if ("for(int i = 0; i < ?; i++) {".Equals (functionName)) {
			ForStart fs = gameObject.GetComponent<ForStart> ();
			StringBuilder sb = new StringBuilder (functionName);
			sb [functionName.IndexOf ('?')] = (char)((char)fs.iterations + '0');
			string newText = sb.ToString ();
			Text text = transform.Find ("Text").GetComponent<Text> ();
			newText = newText + " (" + remInstantiations + ")";
			text.text = newText;
		} else {
			Text text = transform.Find("Text").GetComponent<Text> ();
			text.text = functionName + " (" + remInstantiations + ")";	
		}
	}

}
