﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Jump : MonoBehaviour {

	public string desc = "Robot jumps forward two squares";

	public void execute(int delay) {
		GameObject roboP = GameObject.Find ("RobotPrefab");
		Transform robPref = roboP.transform;
		if (robPref == null) {
			throw new Exception ("Failed to get robot prefab");
		} else {
			Transform robot = robPref.Find ("Robot");
			if (robot == null) {
				throw new Exception ("Failed to get robot");
			} else {
				CharControler cont = robot.GetComponent<CharControler> ();
				if (cont == null) {
					throw new Exception ("Failed to get CharControler");
				} else {
					cont.Invoke("jumpForward", delay);
				}
			}
		}
	}
}
