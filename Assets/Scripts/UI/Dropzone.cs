﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class Dropzone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler  {

	public int tiles = 0;
	public GameObject placeholder = null;
	public int TILE_LIMIT = 5;

	public void OnPointerEnter(PointerEventData eventData)
	{
		GameObject pd = eventData.pointerDrag;
		if (pd != null) {
			UiTile ui = pd.GetComponent<UiTile> ();
			if (ui != null && ui.remInstantiations > 0) {
				LayoutElement le = pd.GetComponent<LayoutElement> ();
				if (le != null && placeholder == null) {
					addPlaceholder (eventData);
				}	
			}
		}
	}

	public void addPlaceholder(PointerEventData eventData) {
		placeholder = new GameObject();
		placeholder.name = "Placeholder";
		placeholder.transform.SetParent (gameObject.transform, false);
		LayoutElement placeholderLe = placeholder.AddComponent<LayoutElement> ();
		placeholderLe.preferredWidth = eventData.pointerDrag.GetComponent<LayoutElement> ().preferredWidth;
		placeholderLe.preferredHeight = eventData.pointerDrag.GetComponent<LayoutElement> ().preferredHeight;
		placeholderLe.flexibleWidth = 0;
		placeholderLe.flexibleWidth = 0;
		placeholder.transform.SetSiblingIndex(eventData.pointerDrag.transform.GetSiblingIndex());
	}

	public void OnPointerExit(PointerEventData eventData)
	{
	}
		
	public void OnDrop(PointerEventData eventData) {
		UiTile u = eventData.pointerDrag.GetComponent<UiTile> ();
		if (u != null) {
			u.endPoint = gameObject.transform;	
		}
	}

}
