﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ForStart : MonoBehaviour {

	public int iterations;
	public string desc = "Initiates a for loop";

	public int delay = 0;
}