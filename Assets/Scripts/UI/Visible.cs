﻿using UnityEngine;
using System.Collections;

public class Visible : MonoBehaviour {

	public GameObject menu;
    public GameObject GameOverMenu;

    void Start() {
		hide ();
	}
		
	public void show() {
		menu.SetActive(true);
	}

	public void hide() {
		menu.SetActive(false);
        GameOverMenu.SetActive(false);

    }
}