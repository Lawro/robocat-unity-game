﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class Trash : MonoBehaviour, IDropHandler {

	public void OnDrop(PointerEventData eventData)
	{
		UiTile ui = eventData.pointerDrag.GetComponent<UiTile> ();
		if (ui != null && ui.remInstantiations > 0) {
			Transform dt = transform.parent.Find ("Dropzone");
			if (dt == null) {
				throw new Exception ("Failed to get dropzone game object");
			} else {
				Dropzone d = dt.GetComponent<Dropzone> ();
				if (d == null) {
					throw new Exception ("Failed to get dropzone component");
				} else {
					if (d.placeholder != null) {
						Destroy (d.placeholder);
					}
				}
			}
			Transform dr = transform.parent.Find ("Dragzone");
			for (int i = 0; i < dr.childCount; i++) {
				Transform child = dr.GetChild (i);
				UiTile childUi = child.GetComponent<UiTile> ();
				if (childUi != null) {
					if (ui.functionName.Equals (childUi.functionName)) {
						childUi.remInstantiations++;
						childUi.setTextWLimit ();
						childUi.enable ();
					}
				}
			}
			FindObjectOfType<AudioManager> ().PlaySound ("Rubbish1");
			FindObjectOfType<AudioManager> ().PlaySound ("Rubbish2");
			FindObjectOfType<AudioManager> ().PlaySound ("Rubbish3");
			Destroy (eventData.pointerDrag);
		}
	}
}

