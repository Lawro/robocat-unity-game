using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CopyChildren : MonoBehaviour {

	private Color32 SIDEBAR_TEXT;
	private Color32 TRANSPARENT;

	// Initialise colour constants
	void Start() {
		SIDEBAR_TEXT = new Color32 (251, 255, 0, 255);
		TRANSPARENT = new Color32 (255, 206, 0, 0);
	}

	public void copy() {
		for (int i = 0; i < transform.childCount; i++) {
			Destroy (transform.GetChild (i).gameObject);
		}
		Transform Ui = transform.root.Find ("UI");
		Transform MainWindow = Ui.Find ("MainWindow");
		Transform Panel = MainWindow.Find ("Panel");
		Transform Dropzone = Panel.Find ("Dropzone");
		for (int i = 0; i < Dropzone.childCount; i++) {
			GameObject orig = Dropzone.GetChild (i).gameObject;
			GameObject copy = (GameObject)Instantiate (Resources.Load ("UiTile"));
			copy.GetComponent<Image> ().color = TRANSPARENT;
			Text text = copy.transform.Find("Text").GetComponent<Text> ();
			text.color = SIDEBAR_TEXT;
			text.fontSize = 28;
			LayoutElement le = copy.GetComponent<LayoutElement> ();
			le.preferredWidth = 300;
			copyScripts (orig, copy);
			UiTile origUi = orig.transform.GetComponent<UiTile> ();
			UiTile copyUi = copy.transform.GetComponent<UiTile> ();
			copyUi.functionName = origUi.functionName;
			copyUi.setTextWoLimit ();
			copy.transform.SetParent (transform, true);
			//copy.transform.SetSiblingIndex (transform.GetSiblingIndex ());
		}
	}

	public void copyScripts(GameObject orig, GameObject replacement) {
		if (orig.GetComponent<Move> () != null) {
			replacement.AddComponent<Move> ();
		}
		if (orig.GetComponent<Turn90Deg> () != null) {
			replacement.AddComponent<Turn90Deg> ();
		}
		if (orig.GetComponent<TurnMinus90Deg> () != null) {
			replacement.AddComponent<TurnMinus90Deg> ();
		}
		if (orig.GetComponent<Turn180Deg> () != null) {
			replacement.AddComponent<Turn180Deg> ();
		}
		if (orig.GetComponent<MoveBack> () != null) {
			replacement.AddComponent<MoveBack> ();
		}
		if (orig.GetComponent<Jump> () != null) {
			replacement.AddComponent<Jump> ();
		}
		if (orig.GetComponent<ForStart> () != null) {
			replacement.AddComponent<ForStart> ();
			replacement.GetComponent<ForStart>().iterations = orig.GetComponent<ForStart>().iterations;
		}
		if (orig.GetComponent<ForEnd> () != null) {
			replacement.AddComponent<ForEnd> ();
		}
	}


}
