﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragzone : MonoBehaviour {

	public void increment(string target) {
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild (i);
			UiTile childUi = child.GetComponent<UiTile> ();
			if (childUi != null) {
				if (target.Equals (childUi.functionName)) {
					childUi.remInstantiations++;
					childUi.setTextWLimit ();
					childUi.enable ();
				}
			}
		}
	}
}
