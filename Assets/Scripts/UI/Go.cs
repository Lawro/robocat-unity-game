﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Go : MonoBehaviour {

	public int compilesAllowed;
	CompilerError CompileError = null;

	void Start() {
		Transform Ui = transform.root.Find ("UI");
		if (Ui == null) {
			throw new Exception ("Failed to get UI");
		} else {
			if (Ui.Find ("Enabler") == null) {
				print ("not finding enabler");
			}
			GameObject enabler = Ui.Find ("Enabler").gameObject;
			if (enabler == null) {
				throw new Exception ("Failed to get enabler");
			} else {
				CompileError = enabler.GetComponent<CompilerError> ();
				if (CompileError == null) {
					throw new Exception ("Failed to get compiler error component of enabler");
				}
			}
		}
	}

	public void execute() {
		if (compilesAllowed <= 0) {
			checkForCompileDeath (0);
		} else {
			compilesAllowed--;
			for (int i = 0; i < transform.childCount; i++) {
				UiTile u = transform.GetChild (i).GetComponent<UiTile> ();
				if ("}".Equals (u.functionName)) {
					CompileError.showError ("For loop cannot end before it starts.");
					return;
				} else if ("for(int i = 0; i < ?; i++) {".Equals (u.functionName)) {
					forExecute (i);
					return;
				}
			}
			linearExecute (0, transform.childCount, 0);
			if (compilesAllowed <= 0) {
				checkForCompileDeath (transform.childCount + 2);
			}
			updateButton ();
		}
	}

	private void updateButton() {
		Transform goBtn = transform.parent.Find ("GoButton");
		if (goBtn == null) {
			throw new Exception ("Failed to get go button");
		} else {
			Text text = goBtn.Find("Text").GetComponent<Text> ();
			text.text = "GO (" + compilesAllowed + ")";
		}
	}
 
	private void checkForCompileDeath(int delay) {
		Transform roboP = transform.root.Find ("RobotPrefab");
		if (roboP == null) {
			throw new Exception ("Failed to get robot prefab");
		} else {
			GameObject robot = roboP.Find ("Robot").gameObject;
			if (robot == null) {
				throw new Exception ("Failed to get robot");
			} else {
				RobotBattery rb = robot.GetComponent<RobotBattery> ();
				if (rb == null) {
					throw new Exception ("Failed to get robot battery component");
				} else {
					rb.Invoke("CompileDeath", delay);
				}
			}
		}
	}
	
	private int linearExecute(int start, int end, int delay) {
		for (int i = start; i < end; i++) {
			UiTile u = transform.GetChild (i).GetComponent<UiTile> ();
			u.execute(delay);
			delay = delay + 1;
		}
		return delay;
	}

	private void forExecute(int forStartIndex) {
		int delay = linearExecute (0, forStartIndex, 0);
		int forEnd = -999;
		for (int i = forStartIndex + 1; i < transform.childCount; i++) {
			UiTile u = transform.GetChild (i).GetComponent<UiTile> ();
			if (u.functionName.Equals ("}")) {
				forEnd = i;
			}
		}
		if (forEnd == -999) {
			CompileError.showError ("For loop not terminated properly.");
		} else {
			UiTile u = transform.GetChild (forStartIndex).GetComponent<UiTile> ();
			ForStart fs = u.GetComponent<ForStart>();
			int iterations = fs.iterations;
			for (int i = 0; i < iterations; i++) {
				print (i);
				delay = linearExecute (forStartIndex + 1, forEnd, delay);		
			}
			linearExecute (forEnd + 1, transform.childCount, delay);
			if (compilesAllowed <= 0) {
				checkForCompileDeath (transform.childCount + 2);
			}
			updateButton ();
		}
	}

}
